import "semantic-ui-css/semantic.min.css";
import { useState } from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from "semantic-ui-react";

function App() {
  const initState = { city1: "", city2: "" };
  const [inputs, setInputs] = useState(() => initState);
  const [distance, setDistance] = useState(null);
  const [time, setTime] = useState(null);
  const API_KEY = "AIzaSyB7tlR7MvVuP7kXdjJtgO0PESGIjbOqRTk";

  const getDistance = async e => {
    let url = "/distancematrix/json?";
    let params = `origins=${inputs.city1}&destinations=${inputs.city2}&key=${API_KEY}`;
    console.log(url + params);

    try {
      let response = await fetch(url + params, {
        method: "GET",
        mode: "cors",
        headers: {
          "Access-Control-Allow-Origin": "*"
        }
      });
      let responseJson = await response.json();
      console.log(responseJson.rows[0].elements[0]);
      let newDistance = responseJson.rows[0].elements[0].distance.text
      let newTime = responseJson.rows[0].elements[0].duration.text
      setDistance(newDistance)
      setTime(newTime)
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    console.log(inputs.city1, inputs.city2);
    getDistance();
  };

  const handleChange = e => {
    const { name, value } = e.target;

    setInputs(prev => ({ ...prev, [name]: value }));
    console.log(inputs);
  };

  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="olive" textAlign="center">
          Calculate Distance Between Cities
        </Header>
        <Form size="large">
          <Segment stacked>
            <Form.Input
              fluid
              icon="building outline"
              iconPosition="left"
              placeholder="First City"
              onChange={handleChange}
              name="city1"
              value={inputs.city1}
            />
            <Form.Input
              fluid
              icon="building outline"
              iconPosition="left"
              placeholder="Second City"
              onChange={handleChange}
              name="city2"
              value={inputs.city2}
            />

            <Button color="olive" fluid size="large" onClick={handleSubmit}>
              Distance
            </Button>
          </Segment>
        </Form>
        {
          distance && (
            <Message>
              {inputs.city1} is {distance} away from {inputs.city2} and it would take you {time} to travel between the two. 
            </Message>
          )
        }
      </Grid.Column>
    </Grid>
  );
}

export default App;
